# Messages

Los mensajes están en `src/messages`, los cuales son ejecutados cuando el regex definido coincide para el mensaje. Para agregar uno nuevo, simplemente segui estos pasos:

## Crear módulo
Creá tu archivo `mimensaje.py` en el directorio mencionado arriba (podés tomar de ejemplo a `sale_peli.py`, que se ejecuta cuando alguien escribe _sale peli?_ o _peli?_), definí al inicio un regex, por ejemplo `REGEX = "^(?:sale )?peli\?$"`, y finalmente definí un método `run(context)`, el cual es ejecutado cuando el usuario envía un mensaje en Telegram que coincide con el regex.

## Agregar módulos de python
Si tu módulo importa alguna dependencia nuevas, por favor agregala al archivo `requirements.txt`