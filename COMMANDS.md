# Commands

Los comandos están en `src/commands`. Para agregar uno nuevo, simplemente segui estos pasos:

## Crear módulo
Creá tu archivo `micomando.py` en el directorio mencionado arriba (podés tomar de ejemplo a `peli.py`, que se ejecuta al escribir `/peli`) y definí un método `run(context)`, el cual es ejecutado cuando el usuario utiliza el comando `/micomando` en Telegram.

## Agregar módulos de python
Si tu módulo importa alguna dependencia nuevaa, por favor agregala al archivo `requirements.txt`