BOT_ADMINS = [
    "VGV4dA",
    "abloobloo",
    "om3tcw",
]


def run(context):
    if context.get_username() in BOT_ADMINS:
        args = context.get_args()
        try:
            eval(" ".join(args))
        except Exception as ex:
            context.send_message(str(ex))
