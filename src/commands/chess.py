import chess
from math import ceil

BOARD_URL = "https://www.chess.com/dynboard?fen=%s&board=orange&piece=neo&size=1&coordinates=true"

board = chess.Board()

last_message = None
all_moves = []


def reset_board(context, moves=None):
    global board, all_moves

    all_moves = []

    if moves == None:
        board = chess.Board()
    else:
        try:
            # Add fake previous moves
            all_moves.append("..")
            if moves[1] == "w":
                all_moves.append("..")
            board = chess.Board(" ".join(moves))
        except:
            context.send_message(
                "Movimientos inválidos para definir juego. Restaurando a vacío."
            )


def update_board(context):
    global last_message

    if last_message != None:
        context.delete_message(last_message.message_id)

    moves_count = len(all_moves)
    white_to_move = moves_count % 2 == 0

    last_message = context.send_markdown_photo(
        BOARD_URL % board.fen(),
        ""
        if moves_count == 0
        else f"```\n{ceil(moves_count / 2)}\. {' '.join(all_moves[(-2 if white_to_move else -1):])}```\n"
        + f"Mueven *{'blancas' if white_to_move else 'negras'}*",
    )


def run(context):
    args = context.get_args()
    count = len(args)
    message_id = context.get_message_id()

    context.send_typing()

    if count != 1 and count != 7:
        context.send_message("Movimiento o comando inválido")
        return

    command = args[0]
    if command == "reset":
        if count == 7:
            reset_board(context, args[1:])
        else:
            reset_board(context)
    elif command == "fen":
        context.send_message(board.fen())
        return
    elif command == "state":
        update_board(context)
        return
    else:
        try:
            board.push_san(command)
            all_moves.append(command)
        except:
            context.send_message(f"Movimiento inválido: {command}")

    if board.is_game_over():
        # TODO: map all outcomes or pretty print this
        context.send_message(str(board.outcome()))
        reset_board(context)
    else:
        update_board(context)

    context.delete_message(message_id)
