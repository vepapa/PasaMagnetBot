import libtorrent as lt
import math, tempfile, string, random
from time import sleep
from dotenv import load_dotenv
from os import environ as ose

load_dotenv()

REGEX = "^magnet:\?xt=urn:[a-z0-9]+:[a-z0-9]{32,40}&dn=.+&tr=.+$"

MAGNET_SAVE_PATH = ose.get("MAGNET_SAVE_PATH")
MAGNET_SAVE_URL = ose.get("MAGNET_SAVE_URL")
TIMEOUT_SECONDS = 30


def id_generator():
    return "".join(random.choice(string.ascii_letters) for _ in range(10))


def get_torrent_size(size_bytes):
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])


def save_magnet(context, magnet, user_name):
    context.send_typing()

    filename = id_generator()
    file_path = MAGNET_SAVE_PATH + filename

    with open(file_path, "w") as file:
        file.write(magnet)

    return "%s \n(%s)" % (MAGNET_SAVE_URL + filename, user_name)


def get_metadata(context, magnet):
    ses = lt.session()
    params = {
        "save_path": tempfile.mkdtemp(),
        "storage_mode": lt.storage_mode_t(2),
        "file_priorities": [0] * 5,
    }

    handle = lt.add_magnet_uri(ses, magnet, params)

    metadata = None
    for i in range(0, TIMEOUT_SECONDS):
        if handle.has_metadata():
            context.send_typing()
            metadata = handle.get_torrent_info()
            ses.remove_torrent(handle)
            break

        sleep(1)

    return metadata


def run(context):
    magnet = context.get_message()
    message_id = context.get_message_id()
    username = context.get_username()

    context.delete_message(message_id)

    save_magnet_response = save_magnet(context, magnet, username)

    updated_message = context.send_message(save_magnet_response)

    metadata = get_metadata(context, magnet)
    if metadata != None:
        size = get_torrent_size(metadata.total_size())
        metadata_text = "%s (%s)" % (metadata.name(), size)
        new_text = "%s\n%s" % (metadata_text, updated_message.text)
        context.edit_message(updated_message.message_id, new_text)
