def strip_iterable(iterable):
    return list(map(str.strip, iterable))