import logging, re
from types import ModuleType
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import ChatAction
from dotenv import load_dotenv
from os import environ as ose, listdir
from os.path import isfile, join
from commands import *
from messages import *

load_dotenv()

BOT_TOKEN = ose.get("BOT_TOKEN")
SEPARATOR = "@"
COMMANDS_FOLDER = "commands"
MESSAGES_FOLDER = "messages"

command_modules = {}
messages_modules = {}


class BotContext:
    def __init__(self, context, update):
        self.context = context
        self.update = update

    def send_typing(self):
        self.context.bot.send_chat_action(self.get_chat_id(), action=ChatAction.TYPING)

    def send_photo(self, url, caption=None):
        return self.context.bot.send_photo(
            chat_id=self.get_chat_id(), caption=caption, photo=url
        )

    def send_markdown_photo(self, url, caption=None):
        return self.context.bot.send_photo(
            chat_id=self.get_chat_id(),
            caption=caption,
            photo=url,
            parse_mode="MarkdownV2",
        )

    def send_message(self, text):
        return self.context.bot.send_message(self.get_chat_id(), text=text)

    def send_markdown_message(self, text):
        return self.context.bot.send_message(
            self.get_chat_id(), text=text, parse_mode="MarkdownV2"
        )

    def send_sticker(self, sticker):
        self.context.bot.send_sticker(self.get_chat_id(), sticker=sticker)

    def edit_message(self, message_id, text):
        return self.context.bot.edit_message_text(text, self.get_chat_id(), message_id)

    def delete_message(self, message_id):
        self.context.bot.delete_message(self.get_chat_id(), message_id)

    def get_chat_id(self):
        return self.update.effective_chat.id

    def get_message_id(self):
        return self.update.message.message_id

    def get_username(self):
        return self.update.message.from_user.username


class BotMessage(BotContext):
    def build(update, context):
        bot_message = BotMessage(context, update)
        message = bot_message.get_message()
        for module in messages_modules.values():
            if re.match(module.REGEX, message, re.IGNORECASE | re.DOTALL):
                module.run(bot_message)

    def __init__(self, context, update):
        super(BotMessage, self).__init__(context, update)

    def get_message(self):
        return self.update.message.text


class BotCommand(BotContext):
    def build(update, context):
        command = update.message.text[1:].split(SEPARATOR)[0].split(" ")[0]
        bot_command = BotCommand(context, update, context.args)
        command_modules[command].run(bot_command)

    def __init__(self, context, update, args=[]):
        super(BotCommand, self).__init__(context, update)
        self.args = args

    def get_args(self):
        return self.args


def __filter_modules_path(modules, path_match):
    return {
        key: value for (key, value) in modules.items() if path_match in value.__file__
    }


def __load_modules():
    modules = {}
    for key in globals():
        maybe_module = globals()[key]
        if isinstance(maybe_module, ModuleType) and key not in ["__builtins__"]:
            modules[key] = maybe_module
    return modules


def run():
    global command_modules, messages_modules

    updater = Updater(token=BOT_TOKEN, use_context=True)
    for c in [
        f.replace(".py", "")
        for f in listdir(COMMANDS_FOLDER)
        if isfile(join(COMMANDS_FOLDER, f)) and ".py" in f and "__init__" not in f
    ]:
        updater.dispatcher.add_handler(CommandHandler(c, BotCommand.build))

    updater.dispatcher.add_handler(MessageHandler(Filters.text, BotMessage.build))

    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )

    modules = __load_modules()
    command_modules = __filter_modules_path(modules, COMMANDS_FOLDER)
    messages_modules = __filter_modules_path(modules, MESSAGES_FOLDER)

    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    run()
