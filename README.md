# PapaMagnetBot

Bot de pelis para el [peliclub](https://vepapa.club)

## Instalación
`pip install -r requirements.txt`

Renombrar archivo `.env.tmp` a `.env` y modificar lo pertinente

## Ejecución
`cd src && python bot.py`
